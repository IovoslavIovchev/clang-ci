#include<iostream>
#include<cassert>

#include "lib.h"
#include "two.h"

int main() {
  assert(is_true());

  assert(one() == 1);

  assert(test() == 4);

  std::cout << "Success" << std::endl;

  return 0;
}
