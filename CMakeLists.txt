cmake_minimum_required(VERSION 3.0)
project(clang_ci)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ansi -pedantic-errors -Wall")

include_directories(src)
include_directories(test)

enable_testing()

file(GLOB EXEC
    "test/test.cpp"
)

file(GLOB SRC
    "src/*.h"
    "src/*.cpp"
)

add_executable(
    clang_ci
    ${EXEC}
    ${SRC}
)

# Uses main() in test.cpp to run tests
add_test(
    NAME clang_ci
    COMMAND clang_ci
)